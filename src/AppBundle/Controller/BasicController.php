<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class BasicController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        $dishesByPlaces = [];
        $places = $this->getDoctrine()->getRepository('AppBundle:Place')->findAll();
        foreach ($places as $place){
            $dish =
                $this->getDoctrine()
                     ->getRepository('AppBundle:Dish')
                     ->getMostPopularDishesByPlace($place, 2);
            $dishesByPlaces[] = [
                'place' => $place,
                'dishes' => $dish,
            ];
        }

        return $this->render('default/index.html.twig', [
            'dishesByPlaces' => $dishesByPlaces
        ]);
    }

    /**
     * @Route("/dishes/{id}", name="dishes", requirements={"id": "\d+"})
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listDishesAction(int $id)
    {
            $dishes =
                $this->getDoctrine()
                    ->getRepository('AppBundle:Dish')
                    ->getListDishesPlace($id);
        dump($dishes);

        return $this->render('default/dishes.html.twig', [
            'dishes' => $dishes
        ]);
    }
}
